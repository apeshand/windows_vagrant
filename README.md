# Инструкша

## Коротенькое описание

Ранее не приходилось поднимать Vagrant с Windows в качестве хостовой системы. Внутри [Vagrantfile](.Vagrantfile) пример с 2 машинами поднимаемыми в цикле и отдельной машиной. Так же - все машины поднимаются в бридже с адаптером хоста, если нет в этом нужды, то удалите строки начинающиеся с `vm_config.vm.network` [17](./Vagrantfile#L17)/[32](./Vagrantfile#L32).

## Установка

1. Установка Vagrant c зеркала [Яндекса](https://hashicorp-releases.yandexcloud.net/vagrant)
2. Добавить `.box` для Вагранта:
    Удалось добавить `.box` нормально только с ключами `--insecure`/`--name`:

    ```bash
    $ vagrant box add https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-vagrant.box --insecure --name jammy-server-cloudimg-amd64-vagrant
    ```

3. Установить плагин для работы с `env`-файлами:

    ```bash
    $ vagrant plugin install vagrant-env
    ```

4. Раздобыть `oscdimg.exe`. Удалось найти этот инструмент [вот тут](https://michael.anastasiou.me/how-to-create-a-bootable-windows-dvd/) и прямая ссылка на архив [эта](https://michael.anastasiou.me//matysoft/oscdimg.zip), нужен для cloud-init, сложить по путям `PATH`.
   - пример для powershell:

    ```powershell
    PS > $env:Path -split ";"
    C:\WINDOWS\system32
    C:\WINDOWS
    C:\WINDOWS\System32\WindowsPowerShell\v1.0\
    C:\Program Files\Git\cmd
    C:\HashiCorp\Vagrant\bin
    C:\Program Files\Oracle\VirtualBox
    ```

   - пример для cmd:

    ```cmd
    > for %a in ("%path:;=";"%") do @echo %~a
    C:\WINDOWS\system32
    C:\WINDOWS
    C:\WINDOWS\System32\WindowsPowerShell\v1.0\
    C:\Program Files\Git\cmd
    C:\HashiCorp\Vagrant\bin
    C:\Program Files\Oracle\VirtualBox
    ```

5. Так же в файле [cloud-init-test.yml](./cloud-init-test.yml#L7) в секции `password` необходимо указать hash вашего пароля, иначе же использовать инструкцию `plain_text_passwd: 'your_plain_text_pass'`.
    - В случае использования хэшированного пароля используйте следующие команды:

      ```bash
      $ sudo apt install -y whoami
      $ mkpasswd --method=SHA-512 --rounds=4096
      Password:
      $6$rounds=4096$3xLf.caE6Gg6E6sW$dO4dmsoNxLbZtSvu0QFbCdQTPBUjLnt30xElg7ZmyWskRE0PB7f0ITikG32UE5dGtdkIplxwP6Cn9nEZs/Ey5/
      ```

    - Если установлен [Git for Windows](https://github.com/git-for-windows/git/releases), то в комплекте скорее всего будет утилита `mkpasswd.exe`, которая совсем и вовсе не аналог подобной утилите `mkpasswd` в linux. Воспользуйтесь любым удобным местом 

## Содержимое репозитория

- [.env](.env) - настройка переменных окружения для Vagrant;
- [cloud-init-test.yml](.cloud-init-test.yml) - файл настройки cloud-init, настройте перед запуск виртуальных машин;
- [README.md](.README.md) - собственно ровно то, что сейчас и открыто и читаемо(какая неожиданность, правда?);
- [oscdimg.zip](.oscdimg.zip) - решил добавить этот файл, чтобы у него не было возможности потеряться;
- [Vagrantfile](.Vagrantfile) - что же это могло бы быть? Ума не приложу!

---

Всё, на этом этапе должно быть возможным поднять виртуалки внутри винды без каких-либо проблем.
